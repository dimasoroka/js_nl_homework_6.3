import React, { useEffect, useState } from 'react';
import routeMain from './routes';
import getPosts from 'services/getPosts';
import Loader from 'components/Loader';
import PostsList from 'components/PostsList';
import PageTitle from 'components/PageTitle/PageTitle';

const MainPage = () => {
  const [postsList, setPostsList] = useState([]);

  useEffect(() => {
    getPosts().then((response) => {
      setPostsList(response.data);
    });
  }, []);

  return (
    <section className="main-page">
      <PageTitle
        title={
          <>
            Posts-<span>site</span>
          </>
        }
      />
      {postsList.length > 0 ? <PostsList list={postsList} /> : <Loader />}
    </section>
  );
};

export { routeMain };
export default MainPage;
