import React from 'react';
import routeInfo from './routes';
import PageTitle from 'components/PageTitle/PageTitle';
import './styles.scss';

const InfoPage = () => (
  <section className="info-page">
    <PageTitle title="Dmytro Soroka" />
    <p className="info-text">Homework WayUp lesson 6.2</p>
  </section>
);

export { routeInfo };
export default InfoPage;
