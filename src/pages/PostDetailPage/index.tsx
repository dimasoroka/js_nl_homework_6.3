import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import routeDetail from './routes';
import getOnePost from 'services/getOnePost';
import Loader from '../../components/Loader';
import PageTitle from 'components/PageTitle/PageTitle';

import { Iid } from 'types/Iid';
import { IPostsDetail } from 'types/IPostsDetail';

import './styles.scss';

const PostDetailPage = () => {
  const { id } = useParams<Iid>();
  const [post, setPost] = useState<IPostsDetail | null>(null);

  useEffect(() => {
    getOnePost(id).then((response) => {
      setPost(response.data);
    });
  }, [id]);

  return (
    <section className="postDetail-page">
      {post ? (
        <>
          <PageTitle title={post.title} />
          <p className="post-text">{post.body}</p>
        </>
      ) : (
        <Loader />
      )}
    </section>
  );
};

export { routeDetail };

export default PostDetailPage;
