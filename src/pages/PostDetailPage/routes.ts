import { TRouteDetail } from 'types/TRouteDetail';

const routeDetail: TRouteDetail = (id = ':id') => `/postDetail/${id}`;

export default routeDetail;
