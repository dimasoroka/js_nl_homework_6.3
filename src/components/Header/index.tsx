import React from 'react';
import { NavLink } from 'react-router-dom';
import { routeMain } from '../../pages/MainPage';
import { routeInfo } from '../../pages/InfoPage';
import './styles.scss';

const Header = () => (
  <header className="header">
    <NavLink to={routeMain()} className="header__logo">
      Posts-site
    </NavLink>
    <nav className="header__menu">
      <NavLink to={routeMain()} activeClassName={'header__active'}>
        Main
      </NavLink>
      <NavLink to={routeInfo()} activeClassName={'header__active'}>
        Info
      </NavLink>
    </nav>
  </header>
);

export default Header;
