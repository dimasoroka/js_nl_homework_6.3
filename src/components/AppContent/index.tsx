import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import MainPage, { routeMain } from '../../pages/MainPage';
import InfoPage, { routeInfo } from 'pages/InfoPage';
import PostDetail, { routeDetail } from 'pages/PostDetailPage';
import Header from 'components/Header';
import './styles.scss';

const AppContent = () => (
  <div className="container">
    <Header />
    <main>
      <Switch>
        <Route exact path={routeMain()} component={MainPage} />
        <Route exact path={routeInfo()} component={InfoPage} />
        <Route exact path={routeDetail()} component={PostDetail} />
        <Redirect
          to={{
            pathname: routeMain(),
          }}
        />
      </Switch>
    </main>
  </div>
);

export default AppContent;
