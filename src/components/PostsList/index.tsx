import React from 'react';
import PostItem from 'components/PostItem';

import { IPostsDetail } from '../../types/IPostsDetail';

import './styles.scss';

interface IPostsListParams {
  list: IPostsDetail[];
}

const PostsList: React.FC<IPostsListParams> = ({ list }) => (
  <div className="posts">
    {list.map((post: IPostsDetail) => {
      return <PostItem item={post} key={post.id} />;
    })}
  </div>
);

export default PostsList;
