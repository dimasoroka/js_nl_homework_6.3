import React from 'react';
import { NavLink } from 'react-router-dom';
import { routeDetail } from '../../pages/PostDetailPage';

import { IPostsDetail } from '../../types/IPostsDetail';

import './styles.scss';

interface IPostsItemParams {
  item: IPostsDetail;
}

const PostItem: React.FC<IPostsItemParams> = ({ item }) => (
  <NavLink to={routeDetail(item.id)} className="post">
    <p className="post__title">{item.title}</p>
  </NavLink>
);

export default PostItem;
