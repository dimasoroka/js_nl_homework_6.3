export interface IPostsDetail {
  userId: number;
  id: number;
  title: string;
  body: string;
}
