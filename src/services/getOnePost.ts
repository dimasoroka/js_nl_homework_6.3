import axios, { AxiosResponse } from 'axios';

const getOnePost = (id: string): Promise<AxiosResponse> => {
  return axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
};

export default getOnePost;
